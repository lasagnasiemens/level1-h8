#include <spring\Application\BaseScene.h>

#include "ui_base_scene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

        //connect btn's release signal to defined slot
        QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));

		QObject::connect(ui->startButton, SIGNAL(released()), this, SLOT(mf_StartTimer()));

		QObject::connect(ui->stopButton, SIGNAL(released()), this, SLOT(mf_StopTimer()));

		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("new title goes here"));

		//setting centralWidget
		centralWidget = ui->centralwidget;

		// TODO REMANE WIDGET
		mv_customPlot = ui->customPlot;

		mp_InitPlot();

		//Get the title from transient data
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		// Get the refresh rate
		mv_dfRefreshRate = boost::any_cast<double>(m_TransientDataCollection["RefreshRate"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));

		mv_timer.setInterval(1000 / mv_dfRefreshRate);
		QObject::connect(&mv_timer, SIGNAL(timeout()), this, SLOT(mf_PlotRandom()));


		// Audio
		// Get the SampleRate
		const unsigned int lv_nSampleRate = boost::any_cast<unsigned int>(m_TransientDataCollection["SampleRate"]);
		mMonoInput = new MonoInput(lv_nSampleRate);

	}

	void BaseScene::mp_InitPlot()
	{
		mv_customPlot->setInteraction(QCP::iRangeZoom, true);
		mv_customPlot->setInteraction(QCP::iRangeDrag, true);

		mv_customPlot->addGraph();
		mv_customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
		mv_customPlot->yAxis->setRange(QCPRange(-1.1, 1.1));
		mv_customPlot->xAxis->setRange(QCPRange(-0.5, 505));

		mv_customPlot->xAxis->setLabel("sec");
		mv_customPlot->yAxis->setLabel("V");
	}

	void BaseScene::release()
	{
		mf_StopTimer();
		delete mAudioInput;
		delete centralWidget;
	}

	BaseScene::~BaseScene()
	{

	}

	void BaseScene::mp_InitMicrophone()
	{
		mMonoInput->open(QIODevice::WriteOnly);
		if (mAudioInput == nullptr)
		{
			mAudioInput = new QAudioInput(mMonoInput->getAudioFormat());
			mAudioInput->start(mMonoInput);
		}
	}

	void BaseScene::mp_StopMicrophone()
	{
		if (mAudioInput != nullptr)
		{
			mAudioInput->stop();
			mMonoInput->close();

			delete mAudioInput;
			mAudioInput = nullptr;
		}
	}

	void BaseScene::mf_StartTimer()
	{
		mp_InitMicrophone();

		mv_timer.start();
		mf_PlotRandom();
	}

	void BaseScene::mf_StopTimer()
	{
		mv_timer.stop();
		mf_CleanPlot();

		mp_StopMicrophone();
	}

  void BaseScene::mp_BackButton()
  {
    const std::string c_szNextSceneName = "Initial scene";
    emit SceneChange(c_szNextSceneName);
  }

  void BaseScene::mf_PlotRandom()
  {
	  const int NO_OF_SAMPLES = 100;

	  QVector<double> vec = mMonoInput->vecGetData();
	  QVector<double> y(NO_OF_SAMPLES);
	  QVector<double> x(vec.length());

	  for (int i = 0; i < vec.length(); i++)
	  {
		  x[i] = i;
		  //y[i] = rand() % 10 + 1;
	  }

	  mv_customPlot->graph(0)->setData(x, vec);
	  //mv_customPlot->rescaleAxes();
	  mv_customPlot->replot();
  }

  void BaseScene::mf_CleanPlot()
  {
	  mv_customPlot->graph(0)->data()->clear();
	  mv_customPlot->replot();
  }
}
