#include <spring\Application\MonoInput.h>
#include <QAudioFormat>
#include <qendian.h>
#include <iostream>

MonoInput::MonoInput(
	const double a_dfSampleRate
	, const int a_nNoOfChannels
	, const int a_nSampleSize
	, const QAudioFormat::SampleType a_eSampleType
    , const QAudioFormat::Endian a_eEndiannes
    , const QString& a_sCodec )
{
	mAudioFormat.setSampleRate(a_dfSampleRate);
	mAudioFormat.setChannelCount(a_nNoOfChannels);
	mAudioFormat.setSampleSize(a_nSampleSize);
	mAudioFormat.setSampleType(a_eSampleType);
	mAudioFormat.setByteOrder(a_eEndiannes);
	mAudioFormat.setCodec("audio/pcm");
}

MonoInput::~MonoInput()
{
}

qint64 MonoInput::readData(char* data, qint64 maxlen) 
{
	Q_UNUSED(data);
	Q_UNUSED(maxlen);
	return -1;
}

qint64 MonoInput::writeData(const char *data, qint64 len)
{
	const auto *ptr = reinterpret_cast<const unsigned char *>(data);
	const int lc_nSampleBytes = mAudioFormat.sampleSize() / 8;
	const int lc_nDataSize = len / lc_nSampleBytes;

	mSamples.clear();
	mSamples.resize(lc_nDataSize);

	for (qint64 j = 0; j < lc_nDataSize; ptr += lc_nSampleBytes, j++)
	{
		const qint16 value = qFromLittleEndian<qint16>(ptr);	
		double level = double(value) / 32767;
		mSamples[j] = level;
	}

	return len;
}

QAudioFormat MonoInput::getAudioFormat()
{
	return mAudioFormat;
}

QVector<double> MonoInput::vecGetData()
{
	return mSamples;
}